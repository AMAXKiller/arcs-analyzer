package com.asusimetest.ArcsAnalyzer;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Point;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnTouchListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.lang.InterruptedException;
import java.lang.Thread;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import paul.arian.fileselector.FileSelectionActivity;

public class MainActivity extends Activity {
    // Constants
    private static final String TAG = "ArcsAnalyzer";
    private static final int REQUEST_CODE_LOAD = 0x1001;
    public static final int MSG_HANDLE_UPDATE = 0x0001;
    public static final int MSG_HANDLE_UPDATE_DONE = 0x0002;
    public static final int MSG_WINDOW_ACTION_DOWN = 0x0003;
    public static final int MSG_WINDOW_ACTION_MOVE = 0x0004;
    // Views
    private static GridView mGridView;
    public static EditText mEdtInput;
    //private TextView mTvDrag;
    // Preview Window
    public static PreviewWindow mPreviewWindow;
    public static int mCurrentX;
    public static int mCurrentY;
    private static float mDx;
    private static float mDy;
    // Local variables
    public static GridViewCustomAdapter mGridViewCustomeAdapter;
    public static ArrayList<File> mScripts;
    public static String sTestFilePath = null;
    private static boolean mUpdateConpeleted;
    // Beginning animation
    private KeywordsFlow keywordsFlow;
    public static final String[] keywords = {
        "腹有詩書氣自華", "一片春愁待酒澆", "明月幾時有", "料峭春風吹酒醒", "庭院深深深幾許",
        "自在飛花輕似夢", "歌盡桃花扇底風", "昨夜星辰昨夜風", "舞低楊柳樓心月", "滿城春色宮牆柳",
        "夕陽無限好", "但願人長久", "夜夜流光相皎潔", "千里共嬋娟", "夜夜金河復玉關",
        "似花還似非花", "若有人知春去處", "此地空餘黃鶴樓", "流水落花春去也", "也無風雨也無晴",
        "問君能有幾多愁", "衣帶漸寬終不悔", "流光容易把人拋", "紅杏枝頭春意鬧", "悲歡離合總無情",
        "回眸一笑百媚生", "桃花流水鮭魚肥", "一江春水向東流", "兩三星火是瓜州", "為賦新詞強說愁",
        "滿園春色關不住", "人生長恨水長東", "美人帳下猶歌舞", "天長地久有時盡", "古道西風瘦馬",
        "花底離愁三月雨", "樓頭殘夢五更鐘"
    };

    private static int mScreenWidth;
    private static int mScreenHeight;
    private static int mStatusBarHeight;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        if (getScreenOrientation() == Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }

        findViews();

        // Load Scripts
        Intent intent_load = new Intent(getApplicationContext(), FileSelectionActivity.class);
        startActivityForResult(intent_load, REQUEST_CODE_LOAD);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mPreviewWindow != null) {
            mPreviewWindow.dismiss();
        }
    }

    private void findViews() {
        mScreenWidth = getApplicationContext().getResources().getDisplayMetrics().widthPixels;
        mScreenHeight = getApplicationContext().getResources().getDisplayMetrics().heightPixels;
        mStatusBarHeight = getApplicationContext().getResources().getDimensionPixelSize(
                getApplicationContext().getResources().getIdentifier("status_bar_height", "dimen", "android"));

        //mTvDrag = (TextView) findViewById(R.id.tv_drag);
        /*mTvDrag.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                if (action == MotionEvent.ACTION_DOWN) {
                    mDx = mCurrentX - event.getRawX();
                    mDy = mCurrentY - event.getRawY();
                }
                else if (action == MotionEvent.ACTION_MOVE) {
                    int ScreenWidth = getResources().getDisplayMetrics().widthPixels;
                    int ScreenHeight = getResources().getDisplayMetrics().heightPixels;
                    int StatusBarHeight = getResources().getDimensionPixelSize(
                            getResources().getIdentifier("status_bar_height", "dimen", "android"));

                    if (!((event.getRawX() + mDx) < 0) &&
                            !(event.getRawX() + mDx + mPreviewWindow.getPreviewViewWidth() > ScreenWidth)) {
                        mCurrentX = (int) (event.getRawX() + mDx);
                    }

                    if (!((event.getRawY() + mDy) < StatusBarHeight) &&
                            !(event.getRawY() + mDy + mPreviewWindow.getPreviewViewHeight() > ScreenHeight)) {
                        mCurrentY = (int) (event.getRawY() + mDy);
                    }

                    mPreviewWindow.update(mCurrentX, mCurrentY, -1, -1);
                }
                return true;
            }
        });
        */

        mEdtInput = (EditText) findViewById(R.id.edt_test);
        mEdtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {

                Log.d("Bohsuan", "afterTextChanged is called");

                if (mUpdateConpeleted) {
                    Log.d("Bohsuan", "mUpdateConpelete is true");
                    if (mPreviewWindow.sRepeatOneIndex != -1) {
                        mGridViewCustomeAdapter.mList3.add(s.toString());

                    } else {
                        mGridViewCustomeAdapter.mList2.add(s.toString());
                    }
                    mGridViewCustomeAdapter.notifyDataSetChanged();

                    if (mPreviewWindow.sRepeatOneIndex == -1) {
                        mGridView.smoothScrollToPosition(mGridViewCustomeAdapter.mList2.size());
                    }

                    mUpdateConpeleted = false;

                    Message msg = new Message();
                    msg.what = MainActivity.MSG_HANDLE_UPDATE_DONE;
                    mUpdateHandler.sendMessage(msg);
                }
                else {
                    Log.d("Bohsuan", "mUpdateConpelete is false");
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        mGridView = (GridView) findViewById(R.id.gridView);
        mGridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View view, int position, long arg3) {
                mPreviewWindow.mImgRepeatOne.setVisibility(View.VISIBLE);
                mPreviewWindow.mImgExport.setVisibility(View.INVISIBLE);
                mPreviewWindow.sRepeatOneIndex = position;
                mPreviewWindow.setTitle(GridViewCustomAdapter.mList.get(position));
                mGridViewCustomeAdapter.mList3.clear();
            }
        });
        keywordsFlow = (KeywordsFlow) findViewById(R.id.keywordsFlow);
        //keywordsFlow.setDuration(800l);
        //feedKeywordsFlow(keywordsFlow, keywords);
        //keywordsFlow.go2Show(KeywordsFlow.ANIMATION_IN);

        mUpdateConpeleted = false;
    }

    private static void feedKeywordsFlow(KeywordsFlow keywordsFlow, String[] arr) {  
        Random random = new Random();  
        for (int i = 0; i < KeywordsFlow.MAX; i++) {  
            int ran = random.nextInt(arr.length);  
            String tmp = arr[ran];  
            keywordsFlow.feedKeyword(tmp);  
        }  
    }  

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        //mMenuSave = menu.findItem(R.id.action_save);
        //mMenuSave.setVisible(false);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_load:
                Intent intent_load = new Intent(getApplicationContext(), FileSelectionActivity.class);
                startActivityForResult(intent_load, REQUEST_CODE_LOAD);
                return true;
            case R.id.action_export:
                if (mGridViewCustomeAdapter.sErrorScripts == null ||
                    mGridViewCustomeAdapter.sErrorScripts.size() == 0) {
                    Toast.makeText(getApplicationContext(),
                            "Error List is empty" ,
                            Toast.LENGTH_LONG).show();
                }
                else {
                    Toast.makeText(getApplicationContext(),
                            "Error List: " + mGridViewCustomeAdapter.sErrorScripts.toString(),
                            Toast.LENGTH_LONG).show();

                    File file = new File(Environment.getExternalStorageDirectory().getPath() + "/" + "ErrorList");

                    try {
                        FileWriter fw = new FileWriter(file);
                        try {
                            for (String str : mGridViewCustomeAdapter.sErrorScripts) {
                                Log.d("Bohsduan", "str = " + str);
                                Log.d("Bohsduan", "no.1 = " + mGridViewCustomeAdapter.mList.get(Integer.valueOf(str)-1));
                                Log.d("Bohsduan", "no.2 = " + mGridViewCustomeAdapter.mList2.get(Integer.valueOf(str)-1));


                                fw.write(str
                                        + " " + mGridViewCustomeAdapter.mList.get(Integer.valueOf(str)-1)
                                        + " " + mGridViewCustomeAdapter.mList2.get(Integer.valueOf(str)-1));
                                fw.write("\r\n");
                            }
                        }
                        catch (IOException e) {
                            e.printStackTrace();
                        }

                        fw.flush();
                        fw.close();

                        MediaScannerConnection.scanFile(this,
                                new String[] { file.getAbsolutePath() }, null, null);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                return true;
            case R.id.action_exit:
                System.exit(0);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (null == data) return;
        switch (requestCode) {
            case REQUEST_CODE_LOAD:
                mScripts = (ArrayList<File>) data.getSerializableExtra("upload");
                for (File file : mScripts ) {
                    if (!IsIMETestingFile(file)) {
                        if (!IsIMEPrefixFile(file)) {
                            Toast.makeText(getApplicationContext(),
                                    getString(R.string.load_file_fail),
                                    Toast.LENGTH_LONG).show();
                            mScripts = null;
                            return;
                        }
                    }
                }

                mGridViewCustomeAdapter = new GridViewCustomAdapter(this);
                mGridView.setAdapter(mGridViewCustomeAdapter);

                keywordsFlow.setVisibility(View.GONE);
                //mTvDrag.setVisibility(View.VISIBLE);
                mEdtInput.setVisibility(View.VISIBLE);
                mGridView.setVisibility(View.VISIBLE);

                //mMenuSave.setVisible(true);

                if (mPreviewWindow == null) {
                    mPreviewWindow = new PreviewWindow(getApplicationContext(),
                        R.layout.preview_window);
                }
                mPreviewWindow.setTitle(GridViewCustomAdapter.mList.get(0));
                //mPreviewWindow.setPosition(position+1);

                /*int ScreenWidth = getResources().getDisplayMetrics().widthPixels;
                int ScreenHeight = getResources().getDisplayMetrics().heightPixels;
                int x = ScreenWidth/2 - mPreviewWindow.getPreviewViewWidth()/2;
                int y = ScreenHeight/2 - mPreviewWindow.getPreviewViewHeight()/2;

                mCurrentX = x;
                mCurrentY = y;
                */
                //mPreviewWindow.showAtLocation(findViewById(R.id.gridView), Gravity.NO_GRAVITY, x, y);

                popupHandler.sendEmptyMessageDelayed(0, 1000);  

            break;
        }
    }

    private Handler popupHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    int ScreenWidth = getApplicationContext().getResources().getDisplayMetrics().widthPixels;
                    int ScreenHeight = getApplicationContext().getResources().getDisplayMetrics().heightPixels;
                    int x = ScreenWidth/2 - mPreviewWindow.getPreviewViewWidth()/2;
                    int y = ScreenHeight/2 - mPreviewWindow.getPreviewViewHeight()/2;

                    mCurrentX = x;
                    mCurrentY = y;

                    mPreviewWindow.showAtLocation(findViewById(R.id.gridView), Gravity.NO_GRAVITY, x, y);
                    mPreviewWindow.update();

                    break;
            }
        }
    };

    private  boolean removeDirectory(File directory) {
        if (directory == null) return false;
        if (!directory.exists()) return true;
        if (!directory.isDirectory()) return false;

        String[] list = directory.list();

        if (list != null) {
            for (int i = 0; i < list.length; i++) {
                File entry = new File(directory, list[i]);

                if (entry.isDirectory()) {
                    if (!removeDirectory(entry)) return false;
                }
                else {
                    if (!entry.delete()) return false;
                }
            }
        }

        return directory.delete();
    }

    public String getUriPath(Uri uri) {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            }
            catch (Exception e) {
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private int getFileNum(File f) {
        File flist[] = f.listFiles();
        return flist.length;
    }

    private boolean IsIMEPrefixFile(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream in = new BufferedInputStream(fis);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String str = reader.readLine();

            if (str.contains("# ASUS IME PREFIX LIST #")) {
                return true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean IsIMETestingFile(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream in = new BufferedInputStream(fis);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String str = reader.readLine();

            if (str.equals("# ASUS IME TESTING SCRIPT #")) {
                return true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static Handler mUpdateHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (MSG_HANDLE_UPDATE == msg.what) {
                Log.d("Bohsuan", "MSG_HANDLE_UPDATE");

                if (!mEdtInput.getText().toString().isEmpty()) {
                    Log.d("Bohsuan", "mEdtInput is not Empty");

                    String title = msg.getData().getString("end_case");
                    if (GridViewCustomAdapter.mPrefixMap.containsKey(title)) {
                        Log.d("Bohsuan", title + " has prefix!");

                        if (GridViewCustomAdapter.mPrefixMap.get(title).equals(mEdtInput.getText().toString())) {
                            mUpdateConpeleted = true;
                        }
                        else {
                            String s = mEdtInput.getText().toString();

                            if (mPreviewWindow.sRepeatOneIndex != -1) {
                                mGridViewCustomeAdapter.mList3.add(s.toString());
                            } else {
                                mGridViewCustomeAdapter.mList2.add(s.toString());
                            }
                            mGridViewCustomeAdapter.notifyDataSetChanged();

                            if (mPreviewWindow.sRepeatOneIndex == -1) {
                                mGridView.smoothScrollToPosition(mGridViewCustomeAdapter.mList2.size());
                            }

                            mEdtInput.setText("");

                            mUpdateConpeleted = false;
                        }
                    }
                    else {
                        String s = mEdtInput.getText().toString();

                        if (mPreviewWindow.sRepeatOneIndex != -1) {
                            mGridViewCustomeAdapter.mList3.add(s.toString());
                        } else {
                            mGridViewCustomeAdapter.mList2.add(s.toString());
                        }
                        mGridViewCustomeAdapter.notifyDataSetChanged();

                        if (mPreviewWindow.sRepeatOneIndex == -1) {
                            mGridView.smoothScrollToPosition(mGridViewCustomeAdapter.mList2.size());
                        }

                        mEdtInput.setText("");

                        mUpdateConpeleted = false;
                    }
                }
                else {
                    Log.d("Bohsuan", "mEdtInput is Empty");
                    mUpdateConpeleted = true;
                }

            }
            else if (MSG_HANDLE_UPDATE_DONE == msg.what) {
                Log.d("Bohsuan", "MSG_HANDLE_UPDATE_DONE");
                mEdtInput.setText("");
            }
        }
    };

    public int getScreenOrientation() {
        Display getOrient = getWindowManager().getDefaultDisplay();
        int orientation = Configuration.ORIENTATION_UNDEFINED;
        if(getOrient.getWidth()==getOrient.getHeight()){
            orientation = Configuration.ORIENTATION_SQUARE;
        } else{ 
            if(getOrient.getWidth() < getOrient.getHeight()){
                orientation = Configuration.ORIENTATION_PORTRAIT;
            }else { 
                orientation = Configuration.ORIENTATION_LANDSCAPE;
            }
        }
        return orientation;
    }

    public static Handler sPreviewWindowHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (MSG_WINDOW_ACTION_DOWN == msg.what) {
                mDx = mCurrentX - msg.getData().getInt("RawX");
                mDy = mCurrentY - msg.getData().getInt("RawY");
            }
            else if (MSG_WINDOW_ACTION_MOVE == msg.what) {
                int RawX = msg.getData().getInt("RawX");
                int RawY = msg.getData().getInt("RawY");

                if (!((RawX + mDx) < 0) &&
                        !(RawX + mDx + mPreviewWindow.getPreviewViewWidth() > mScreenWidth)) {
                    mCurrentX = (int) (RawX + mDx);
                }

                if (!((RawY + mDy) < mStatusBarHeight) &&
                        !(RawY + mDy + mPreviewWindow.getPreviewViewHeight() > mScreenHeight)) {
                    mCurrentY = (int) (RawY + mDy);
                }

                mPreviewWindow.update(mCurrentX, mCurrentY, -1, -1);
            }
        }
    };

}
