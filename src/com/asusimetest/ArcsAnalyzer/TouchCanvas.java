package com.asusimetest.ArcsAnalyzer;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;

public class TouchCanvas extends View {
    private static final String TAG="TouchCanvas";

    private Bitmap  mBitmap;
    private Canvas  mCanvas;
    private Path    mPath;
    private Paint   mBitmapPaint;

    private Paint   mPaint;
    private int mWidthBmp;
    private int mHeightBmp;

    private boolean mIsEnableRecordTime=true;
    protected ArrayList<String> mTouchHistory=new ArrayList<String>(1024);

    private int viewPosX=0;
    private int viewPosY=0;

    private long mlastActionUpTime;

    private float mX, mY;
    private static final float TOUCH_TOLERANCE = 4;

    private long lastTime=0;

    public boolean sIsRecording;
    public int sScreenSize;

    public TouchCanvas(Context context) {
        super(context);
        initResource();
    }

    public TouchCanvas(Context context, AttributeSet attrs) {
        super(context, attrs);
        initResource();
    }

    private void initResource() {
        mPath = new Path();
        mBitmapPaint = new Paint(Paint.DITHER_FLAG);

        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(0xFFFF0000);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(12);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        mWidthBmp=w;mHeightBmp=h;

        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        mCanvas = new Canvas(mBitmap);
    }

    public void clearCanvas() {
        if (mCanvas == null) return;

        Paint paint=new Paint();
        paint.setColor(0xFFFFFFFF);
        mCanvas.drawRect(0,0,mWidthBmp,mHeightBmp, paint);
        mPath.reset();
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawColor(0xFFAAAAAA);
        canvas.drawBitmap(mBitmap, 0, 0, mBitmapPaint);
        canvas.drawPath(mPath, mPaint);
    }

    protected void drawPath(Canvas canvas) {
        canvas.drawPath(mPath, mPaint);
    }

    private void touch_start(float x, float y) {
        mPath.reset();
        mPath.moveTo(x, y);
        mX = x;  mY = y;
    }

    private void touch_move(float x, float y) {
        if (PointF.length(mX-x, mY-y) > TOUCH_TOLERANCE) {
            mPath.quadTo(mX, mY, (x + mX)/2, (y + mY)/2);
            mX = x;  mY = y;
        }
    }

    private void touch_up(float x,float y) {
        mPath.lineTo(mX, mY);
        mCanvas.drawPath(mPath, mPaint);
    }

    void enableRecordTime(boolean _enableOrNot) {
        mIsEnableRecordTime=_enableOrNot;
    }

    public void startRecord() {
        mTouchHistory.clear();
        lastTime=SystemClock.uptimeMillis();
        int[] location=new int[2];
        getLocationOnScreen(location);
        viewPosX=location[0];
        viewPosY=location[1];	
    }

    public void saveRecord(String _testCase, File _input) {
        try {
            FileWriter fw = new FileWriter(_input);
            fw.write("# start " + _testCase + "\r\n");
            for (String rec : mTouchHistory) {
                fw.write(rec);
                fw.write("\r\n");
            }
            fw.write("# end " + _testCase + "\r\n");
            fw.flush();
            fw.close();
        } catch (Exception e) {e.printStackTrace();}
        //mTouchHistory.clear();
    }

    public void clearTouchHistory() {
        mTouchHistory.clear();
        mPath.reset();
        //mPath.reset();
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (!sIsRecording) return false;

        float x = event.getX();
        float y = event.getY();
        super.onTouchEvent(event);
        long curTime=SystemClock.uptimeMillis();
        long cost=curTime-lastTime;
        lastTime=curTime;
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (mIsEnableRecordTime) {
                    if (curTime - mlastActionUpTime > 500) {
                        clearCanvas();
                    }
                }
                touch_start(x, y);
                invalidate();
                if (mIsEnableRecordTime) mTouchHistory.add("sleep "+cost);
                mTouchHistory.add("touch_down "+(viewPosX+(int) x)+","+(viewPosY+(int) y));
                Log.d(TAG,"onTouchEvent ACTION_DOWN ("+(int) x+","+(int) y+") at "+cost);
                break;
            case MotionEvent.ACTION_MOVE:
                touch_move(x, y);
                invalidate();
                if (mIsEnableRecordTime) mTouchHistory.add("sleep "+cost);
                mTouchHistory.add("touch_move "+(viewPosX+(int) x)+","+(viewPosY+(int) y));                   
                Log.d(TAG,"onTouchEvent ACTION_MOVE ("+(int) x+","+(int) y+") at "+cost);
                break;
            case MotionEvent.ACTION_UP:
                touch_up(x,y);
                invalidate();
                if (mIsEnableRecordTime) mTouchHistory.add("sleep "+cost);
                mTouchHistory.add("touch_up "+(viewPosX+(int) x)+","+(viewPosY+(int) y));
                mlastActionUpTime = curTime;
                Log.d(TAG,"onTouchEvent ACTION_UP ("+(int) x+","+(int) y+") at "+cost);
                break;
        }
        return true;
    }
}
