package com.asusimetest.ArcsAnalyzer;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.IOException;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class GridViewCustomAdapter extends ArrayAdapter {
    private Context mContext;
    public static ArrayList<String> mList;
    public static ArrayList<String> mList2;
    public static ArrayList<String> mList3;
    public static ArrayList<String> sErrorScripts;
    public static Map<String, String> mPrefixMap;

    public GridViewCustomAdapter(Context context) {
        super(context, 0);
        mContext = context;

        mList = new ArrayList<String>();

        mList2 = new ArrayList<String>();

        mList3 = new ArrayList<String>();

        sErrorScripts = new ArrayList<String>();

        mPrefixMap = new HashMap<String, String>();

        convertCodeAndGetText();
    }

    public int getCount() {
        return mList.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null)  {
            LayoutInflater inflater = ((Activity)mContext).getLayoutInflater();
            row = inflater.inflate(R.layout.grid_row, parent, false);
        }

        TextView textViewTitle_num = (TextView) row.findViewById(R.id.textView_num);
        textViewTitle_num.setText("" + (position+1));

        TextView textViewTitle = (TextView) row.findViewById(R.id.textView);

        String prefix_text = "";
        if (mPrefixMap.containsKey(mList.get(position))) {
            prefix_text = mPrefixMap.get(mList.get(position));
        }
        textViewTitle.setText(prefix_text + mList.get(position));

        TextView textViewTitle_result = (TextView) row.findViewById(R.id.textView_result);

        row.setBackgroundColor(Color.TRANSPARENT);
        textViewTitle_result.setText("");

        if (position < mList2.size()) {
            textViewTitle_result.setText(mList2.get(position));

            if (mList2.get(position).equals(prefix_text + mList.get(position))) {
                row.setBackgroundColor(Color.GREEN);
            } else {
                row.setBackgroundColor(Color.RED);
                if (!sErrorScripts.contains(Integer.toString(position+1))) {
                    sErrorScripts.add(Integer.toString(position+1));
                }
            }
        }

        if (MainActivity.mPreviewWindow.sRepeatOneIndex != -1) {
            if (position == MainActivity.mPreviewWindow.sRepeatOneIndex) {
                if (!mList3.isEmpty()) {
                    textViewTitle_result.setText(mList3.get(0));
                    row.setBackgroundColor(Color.YELLOW);
                }
            }
        }

        return row;
    }

    private void convertCodeAndGetText() {
        try {
            for (File file : MainActivity.mScripts) {
                BufferedReader reader;
                FileInputStream fis = new FileInputStream(file);
                BufferedInputStream in = new BufferedInputStream(fis);
                in.mark(4);
                byte[] first3bytes = new byte[3]; 
                in.read(first3bytes);
                in.reset();
                /*
                if (first3bytes[0] == (byte) 0xEF && first3bytes[1] == (byte) 0xBB
                        && first3bytes[2] == (byte) 0xBF) {
                    reader = new BufferedReader(new InputStreamReader(in, "utf-8"));
                }
                else if (first3bytes[0] == (byte) 0xFF && first3bytes[1] == (byte) 0xFE) {
                    reader = new BufferedReader(new InputStreamReader(in, "unicode"));
                }
                else if (first3bytes[0] == (byte) 0xFE && first3bytes[1] == (byte) 0xFF) {
                    reader = new BufferedReader(new InputStreamReader(in, "utf-16be"));
                }
                else if (first3bytes[0] == (byte) 0xFF && first3bytes[1] == (byte) 0xFF) {
                    reader = new BufferedReader(new InputStreamReader(in, "utf-16le"));
                }
                else {
                    reader = new BufferedReader(new InputStreamReader(in, "Big5"));
                }
                */

                reader = new BufferedReader(new InputStreamReader(in, "utf-8"));

                if (IsIMEPrefixFile(file)) {
                    String str = reader.readLine();
                    while (str != null) {
                        if (str.contains("@")) {
                            if (!mPrefixMap.containsKey(Character.toString(str.charAt(0)))) {
                                mPrefixMap.put(Character.toString(str.charAt(0)), str.substring(2));
                            }
                        }
                        str = reader.readLine();
                    }
                    reader.close();
                    continue;
                }

                String str = reader.readLine();
                while (str != null) {
                    if (str.startsWith("# start")) {
                        mList.add(str.substring(str.indexOf('=') + 1, str.length()));
                    }
                    str = reader.readLine();
                }
                reader.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean IsIMEPrefixFile(File file) {
        try {
            FileInputStream fis = new FileInputStream(file);
            BufferedInputStream in = new BufferedInputStream(fis);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String str = reader.readLine();

            if (str.contains("# ASUS IME PREFIX LIST #")) {
                return true;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }
}
