package com.asusimetest.ArcsAnalyzer;

import android.os.Message;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.lang.Runnable;
import java.lang.StringBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.StringTokenizer;
import android.os.Bundle;

public class ScriptIntepreter implements Runnable {
    ArrayList<ScriptCommand> script = new ArrayList<ScriptCommand>(1024);
    ArrayList<CommandInvoker> invokers = new ArrayList<CommandInvoker>();
    ScriptDefaultInvoker defaultInvoker = new ScriptDefaultInvoker(script);
    double speed = 1.0;

    private boolean mIsStop = false;

    public void loadFile(File _file) throws IOException {
        FileInputStream fis = new FileInputStream(_file);
        loadScript(fis);
        fis.close();
    }

    public void setSpeed(double _speed) {
        speed = _speed;
    }

    public void loadScript(InputStream _is) throws IOException {
        script.clear();
        parseScript(_is, script);
NextCmd: for (ScriptCommand cmd : script) {
             for (CommandInvoker invoker : invokers) {
                 if (invoker.canSupport(cmd)) {
                     cmd.setInvoker(invoker);
                     continue NextCmd;
                 }
             }
             if (!defaultInvoker.canSupport(cmd)) throw new RuntimeException("Unknown command : "+cmd);
             cmd.setInvoker(defaultInvoker);
         }

         for (ScriptCommand cmd : script) cmd.preprocess();
    }

    public void registerInvoker(CommandInvoker _invoker) {
        invokers.add(_invoker);
    }

    public void run() {
        int cmdEnd=script.size();

        for (int cmdCount=0; cmdCount < cmdEnd; cmdCount++) {
            if (mIsStop) {
                return;
            }

            ScriptCommand cmd=script.get(cmdCount);
            ScriptIntepreter.CommandInvoker invoker=cmd.getInvoker();
            cmd.run();
            if (defaultInvoker == invoker) {
                cmdCount = defaultInvoker.getNextCmdCount(cmdCount);
            }

            PreviewWindow.mPbPlay.setProgress((int)(((float)cmdCount/cmdEnd)*100));
        }

        PreviewWindow.mPbPlay.setProgress(100);
    }

    public void stop() {
        mIsStop = true;
    }

    private void parseScript(InputStream _is, ArrayList<ScriptCommand> _cmds) throws IOException {
        int idxLineNum = 0;
        BufferedReader br = new BufferedReader(new InputStreamReader(_is));
        for (String line = br.readLine(); null != line; line = br.readLine()) {
            idxLineNum++;

            if (line.startsWith("# start")) {
                line = "testcase=" + line.substring(line.indexOf('=') + 1, line.length());
            }

            if (line.startsWith("# end")) {
                line = "endcase=" + line.substring(line.indexOf('=') + 1, line.length());
            }

            if (line.trim().length() <= 0) continue;
            if ('#' == line.charAt(0)) continue;
            _cmds.add(new ScriptCommand(line,idxLineNum,_cmds.size()));
        }
        br.close();
    }

    public class ScriptCommand implements Runnable {
        String cmdLine;
        String[] cmdParas;
        ScriptIntepreter.CommandInvoker invoker;
        int idxLineNum;
        int idxCmdAddr;

        public ScriptCommand(String _cmdLine, int _iLine, int _iAddr) {	
            cmdLine = _cmdLine;
            idxLineNum = _iLine;
            idxCmdAddr = _iAddr;
            cmdParas = parseCommand(cmdLine); 
        } 

        public void preprocess() {
            invoker.preprocess(this);
        }

        public void run() {
            invoker.run(this);
        }

        public double getParameterValue(int _idx) {
            return (cmdParas[_idx].charAt(0) >= 'a') ? defaultInvoker.getVariableValue(cmdParas[_idx]) 
                : new Double(cmdParas[_idx]).doubleValue();
        }

        public void setParameterValue(int _idx,double _value) {
            defaultInvoker.setVariableValue(cmdParas[_idx],_value);
        }

        public boolean isParameterWithValue(int _idx) {
            return defaultInvoker.isVariableWithValue(cmdParas[_idx]);
        }

        public int getLineNumInSource() {
            return idxLineNum;
        }

        public String[] getCommandParameters() {
            return cmdParas;
        }

        private String[] parseCommand(String _cmdLine) {
            final String delimiters="\" ,";
            LinkedList<String> strs = new LinkedList<String>(); 
            StringTokenizer st = new StringTokenizer(_cmdLine, delimiters, true);
            while (st.hasMoreTokens()) {
                String token = st.nextToken();

                if (token.equals(" ") || token.equals(",")) {
                    continue;
                }

                if (token.equals("\"")) {
                    StringBuffer sb = new StringBuffer();
                    while (st.hasMoreTokens()) {
                        token = st.nextToken();
                        if (token.equals("\"")) break;
                        sb.append(token);
                    }
                    strs.add(sb.toString().toLowerCase());
                    continue;
                }
                strs.add(token.toLowerCase());
            }
            return strs.toArray(new String[0]);
        }

        private void setInvoker(ScriptIntepreter.CommandInvoker _invoker) {
            invoker=_invoker;
        }

        private ScriptIntepreter.CommandInvoker getInvoker() {
            return invoker;
        }

        private int getCommandAddress() {
            return idxCmdAddr;
        }

        public String toString() {
            StringBuffer sb = new StringBuffer();
            for (String para : cmdParas) sb.append("["+para+"]");
            return sb.toString();
        }
    } // class ScriptCommand

    private class ScriptDefaultInvoker implements ScriptIntepreter.CommandInvoker {
        ArrayList<ScriptCommand> script;
        HashMap<String,Double> varValueMap = new HashMap<String,Double>();
        int nextCmdAddr = -1;

        public ScriptDefaultInvoker(ArrayList<ScriptCommand> _script) {
            script = _script;
        }

        private boolean isVariableWithValue(String _var) {
            return varValueMap.containsKey(_var);
        }

        private double getVariableValue(String _var) {
            return varValueMap.get(_var).doubleValue();
        }

        private void setVariableValue(String _var,double _value) {
            varValueMap.put(_var, _value);
        }

        public void preprocess(ScriptIntepreter.ScriptCommand _cmd) {
            String[] cmdParas = _cmd.getCommandParameters();
            if (cmdParas[0].equals("label")) {
                _cmd.setParameterValue(1, _cmd.getCommandAddress());
            }
        }

        private int getNextCmdCount(int _curCount) {
            if (nextCmdAddr >= 0) {
                _curCount=nextCmdAddr;
                nextCmdAddr=-1;
            }
            return _curCount;
        }

        public void run(ScriptIntepreter.ScriptCommand _cmd) {
            String[] cmdParas=_cmd.getCommandParameters();

            if (cmdParas[0].startsWith("testcase")) {
                Log.d("Bohsuan", "testcase: " + cmdParas[0]);
                String test_case = cmdParas[0].substring(cmdParas[0].indexOf('=')+1, cmdParas[0].length());
                Bundle bundle = new Bundle();
                bundle.putString("test_case", test_case);
                Message msg = new Message();
                msg.setData(bundle);
                msg.what = PreviewWindow.MSG_UPDATE_WINDOW_TITLE;
                PreviewWindow.mRunDoneHandler.sendMessage(msg);
            }
            else if (cmdParas[0].startsWith("endcase")) {
                Log.d("Bohsuan", "endcase: " + cmdParas[0]);
                String end_case = cmdParas[0].substring(cmdParas[0].indexOf('=')+1, cmdParas[0].length());
                Bundle bundle = new Bundle();
                bundle.putString("end_case", end_case);
                Message msg = new Message();
                msg.setData(bundle);
                msg.what = MainActivity.MSG_HANDLE_UPDATE;
                MainActivity.mUpdateHandler.sendMessageDelayed(msg, 500);
            }
            else if (cmdParas[0].equals("label")) {
            }
            else if (cmdParas[0].equals("add")) {
                _cmd.setParameterValue(1,(_cmd.getParameterValue(1)+_cmd.getParameterValue(2)));
            }
            else if (cmdParas[0].equals("sub")) {
                _cmd.setParameterValue(1,(_cmd.getParameterValue(1)-_cmd.getParameterValue(2)));
            }
            else if (cmdParas[0].equals("mul")) {
                _cmd.setParameterValue(1,(_cmd.getParameterValue(1)*_cmd.getParameterValue(2)));
            }
            else if (cmdParas[0].equals("div")) {
                _cmd.setParameterValue(1,(_cmd.getParameterValue(1)/_cmd.getParameterValue(2)));
            }
            else if (cmdParas[0].equals("mod")) {
                _cmd.setParameterValue(1,(_cmd.getParameterValue(1)%_cmd.getParameterValue(2)));
            }
            else if (cmdParas[0].equals("mov")) {
                _cmd.setParameterValue(1,_cmd.getParameterValue(2));	
            }
            else if (cmdParas[0].equals("brn")) {
                nextCmdAddr=(int) _cmd.getParameterValue(1);
            }
            else if (cmdParas[0].equals("bgt")) {
                nextCmdAddr=(_cmd.getParameterValue(2) > _cmd.getParameterValue(3))? (int)_cmd.getParameterValue(1) : -1;
            }
            else if (cmdParas[0].equals("bge")) {
                nextCmdAddr=(_cmd.getParameterValue(2) >= _cmd.getParameterValue(3))? (int)_cmd.getParameterValue(1) : -1;
            }
            else if (cmdParas[0].equals("blt")) {
                nextCmdAddr=(_cmd.getParameterValue(2) < _cmd.getParameterValue(3))? (int)_cmd.getParameterValue(1) : -1;
            }
            else if (cmdParas[0].equals("ble")) {
                nextCmdAddr=(_cmd.getParameterValue(2) <= _cmd.getParameterValue(3))? (int)_cmd.getParameterValue(1) : -1;
            }
            else if (cmdParas[0].equals("sleep")) {
                try {Thread.sleep((int)(_cmd.getParameterValue(1)/speed));} catch (Exception e) {}
            }
            else if (cmdParas[0].equals("time")) {
                _cmd.setParameterValue(1,System.currentTimeMillis());
            }
            else if (cmdParas[0].equals("print")) {
                StringBuffer sb=new StringBuffer();
                for (int i=1; i < cmdParas.length; i++) sb.append(_cmd.isParameterWithValue(i)? _cmd.getParameterValue(i) : cmdParas[i]); 
                System.out.println(sb.toString());
            } else throw new RuntimeException("Unknow command "+_cmd+" at line "+_cmd.getLineNumInSource());
        }

        public boolean canSupport(ScriptIntepreter.ScriptCommand _cmd) {
            return true;
        }
    } // class ScriptDefaultInvoker

    public interface CommandInvoker {
        public boolean canSupport(ScriptIntepreter.ScriptCommand _cmd);
        public void preprocess(ScriptIntepreter.ScriptCommand _cmd);
        public void run(ScriptIntepreter.ScriptCommand _cmd);
    }
}
