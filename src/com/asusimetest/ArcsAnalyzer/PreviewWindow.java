package com.asusimetest.ArcsAnalyzer;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.media.MediaScannerConnection;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.PopupWindow.OnDismissListener;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.lang.Runnable;
import java.lang.System;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class PreviewWindow extends PopupWindow {
    public static final String TAG = "ArcsAnalyzer";
    public static final int MSG_SCRIPT_RUN_DONE = 0x1001;
    public static final int MSG_UPDATE_WINDOW_TITLE = 0x1002;
    public static final int MSG_SHOW_EXPORT = 0x1003;

    private TextView mPreviewTitle;
    private static ImageButton mBtnExpand;
    private static ImageButton mBtnPlay;
    private static ImageButton mBtnResize;
    private static TouchCanvas mCanvasView;

    public static ImageView mImgRepeatOne;
    public static ImageView mImgExport;

    private float mScale;

    public static ProgressBar mPbPlay_1;
    public static ProgressBar mPbPlay;

    private static View mContentView;
    private Context mContext;

    public static int sScriptPreviewViewWidth;
    public static int sScriptPreviewViewHeight;
    public static int sScriptStartX;
    public static int sScriptStartY;

    public static boolean sIsCanvasHide;
    public static int sScriptsNum;

    public static int sRepeatOneIndex = -1;

    private static File export_file;

    private EditText mScriptName;

    private static boolean sIsPlayMode;

    private static Thread mRunScriptThread = null;

    private static ScriptIntepreter mInterpreter = null;

    public PreviewWindow(Context context, int layout_id) {
        mContext = context;
        mContentView = View.inflate(mContext, layout_id, null);

        sIsPlayMode = false;

        int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        mContentView.measure(w, h);

        setContentView(mContentView);
        setWidth(LayoutParams.WRAP_CONTENT);
        setHeight(LayoutParams.WRAP_CONTENT);

        // workaround to listen the outside touch
        setBackgroundDrawable(new BitmapDrawable());
        setFocusable(false);
        setOutsideTouchable(false);

        mPreviewTitle = (TextView) mContentView.findViewById(R.id.preview_title);

        mPreviewTitle.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                int action = event.getAction();
                if (action == MotionEvent.ACTION_DOWN) {
                    Message m = new Message();
                    m.what = MainActivity.MSG_WINDOW_ACTION_DOWN;
                    Bundle b = new Bundle();
                    b.putInt("RawX", (int) event.getRawX());
                    b.putInt("RawY", (int) event.getRawY());
                    m.setData(b);
                    MainActivity.sPreviewWindowHandler.sendMessage(m);
                }
                else if (action == MotionEvent.ACTION_MOVE) {
                    Message m = new Message();
                    m.what = MainActivity.MSG_WINDOW_ACTION_MOVE;
                    Bundle b = new Bundle();
                    b.putInt("RawX", (int) event.getRawX());
                    b.putInt("RawY", (int) event.getRawY());
                    m.setData(b);
                    MainActivity.sPreviewWindowHandler.sendMessage(m);
                }
                return true;
            }
        });

        mBtnExpand = (ImageButton) mContentView.findViewById(R.id.btn_expand);
        mBtnPlay = (ImageButton) mContentView.findViewById(R.id.btn_play);
        mBtnResize = (ImageButton) mContentView.findViewById(R.id.btn_resize);
        mCanvasView = (TouchCanvas) mContentView.findViewById(R.id.touchCanvas);
        mPbPlay_1 = (ProgressBar) mContentView.findViewById(R.id.pb_play_1);
        mPbPlay = (ProgressBar) mContentView.findViewById(R.id.pb_play);
        mImgRepeatOne = (ImageView) mContentView.findViewById(R.id.img_repeat_one);
        mImgExport = (ImageView) mContentView.findViewById(R.id.img_export);

        mCanvasView.sIsRecording = false;
        mCanvasView.sScreenSize = 300;
        mScale = mContext.getResources().getDisplayMetrics().density;
        sIsCanvasHide = false;

        mImgRepeatOne.setClickable(true);
        mImgRepeatOne.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                mImgRepeatOne.setVisibility(View.INVISIBLE);
                mImgExport.setVisibility(View.INVISIBLE);
                setTitle(GridViewCustomAdapter.mList.get(0));
                sRepeatOneIndex = -1;
            }
        });

        mImgExport.setClickable(true);
        mImgExport.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (export_file != null) {
                    //showSavingDialog();
                    saveScript();
                }
            }
        });


        mBtnExpand.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sIsCanvasHide) {
                    mBtnExpand.setImageResource(R.drawable.ic_action_collapse);
                    mCanvasView.setVisibility(View.VISIBLE);
                    sIsCanvasHide = false;
                } else {
                    mBtnExpand.setImageResource(R.drawable.ic_action_expand);
                    mCanvasView.clearCanvas();
                    mCanvasView.setVisibility(View.GONE);
                    sIsCanvasHide = true;
                }
            }
        });

        mBtnPlay.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!sIsPlayMode) {
                    if (!mCanvasView.sIsRecording) {
                        mCanvasView.sIsRecording = true;
                        mBtnExpand.setEnabled(false);
                        mBtnResize.setEnabled(false);
                        //mBtnPlay.setEnabled(false);
                        mBtnPlay.setImageResource(R.drawable.ic_action_end);
                        mPbPlay_1.setProgress(0);
                        mPbPlay.setProgress(0);
                        sScriptsNum = 0;

                        if (sRepeatOneIndex == -1) {
                            MainActivity.mGridViewCustomeAdapter.mList2.clear();
                            MainActivity.mGridViewCustomeAdapter.sErrorScripts.clear();
                        }

                        runScript();
                        sIsPlayMode = true;
                    }
                }
                else {
                    Log.d(TAG, "Script Stop");
                    mBtnPlay.setImageResource(R.drawable.ic_action_play);
                    if (mRunScriptThread != null) {
                        mRunScriptThread.interrupt();
                    }
                    sIsPlayMode = false;
                    if (mInterpreter != null) {
                        mInterpreter.stop();
                    }
                    mInterpreter = null;
                }
            }
        });

        mBtnResize.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                int dp = 0;

                if (mCanvasView.sScreenSize == 300) {
                    mBtnResize.setImageResource(R.drawable.ic_action_return_from_full_screen);
                    mCanvasView.sScreenSize = 250;
                    dp = (int) mContext.getResources().getDimension(R.dimen.pattern_image_preview_size_250);
                }
                else if (mCanvasView.sScreenSize == 250) {
                    mBtnResize.setImageResource(R.drawable.ic_action_return_from_full_screen);
                    mCanvasView.sScreenSize = 200;
                    dp = (int) mContext.getResources().getDimension(R.dimen.pattern_image_preview_size_200);
                }
                else if (mCanvasView.sScreenSize == 200) {
                    mBtnResize.setImageResource(R.drawable.ic_action_full_screen);
                    mCanvasView.sScreenSize = 150;
                    dp = (int) mContext.getResources().getDimension(R.dimen.pattern_image_preview_size_150);

                }
                else if (mCanvasView.sScreenSize == 150) {
                    mBtnResize.setImageResource(R.drawable.ic_action_return_from_full_screen);
                    mCanvasView.sScreenSize = 300;
                    dp = (int) mContext.getResources().getDimension(R.dimen.pattern_image_preview_size_300);
                }

                mCanvasView.clearCanvas();
                mCanvasView.setLayoutParams(new FrameLayout.LayoutParams(dp, dp));
                mPreviewTitle.setLayoutParams(new FrameLayout.LayoutParams(dp, ViewGroup.LayoutParams.WRAP_CONTENT));
            }
        });
    }

    public void setTitle(String text) {
        mPreviewTitle.setText(text);
        mPreviewTitle.setSelected(true);
    }

    public int getPreviewViewHeight() {
        return mContentView.getMeasuredHeight();
    }

    public int getPreviewViewWidth() {
        return mContentView.getMeasuredWidth();
    }

    private void runScript() {
        mCanvasView.clearCanvas();
        mRunScriptThread = new Thread(mPlayRunnable);
        mRunScriptThread.start();
        //new Thread(mPlayRunnable).start();
    }

    private Runnable mPlayRunnable = new Runnable() {
        public void run () {
            try {
                int index = 0;

                for (File script : MainActivity.mScripts) {
                    if (!sIsPlayMode) {
                        Message msg = new Message();
                        msg.what = MSG_SCRIPT_RUN_DONE;
                        mRunDoneHandler.sendMessage(msg);
                        break;
                    }

                    BufferedReader reader;
                    FileInputStream fis = new FileInputStream(script);
                    BufferedInputStream in = new BufferedInputStream(fis);
                    reader = new BufferedReader(new InputStreamReader(in, "utf-8"));
                    String str = reader.readLine();

                    if (str.contains("# ASUS IME PREFIX LIST #")) {
                        Log.d(TAG, "Is prefix");
                        continue;
                    }

                    while (str != null) {
                        if (str.startsWith("# PreviewViewWidth")) {
                            sScriptPreviewViewWidth = Integer.valueOf(str.substring(str.indexOf('=') + 1, str.length()));
                        }
                        if (str.startsWith("# PreviewViewHeight")) {
                            sScriptPreviewViewHeight = Integer.valueOf(str.substring(str.indexOf('=') + 1, str.length()));
                        }
                        if (str.startsWith("# StartX")) {
                            sScriptStartX = Integer.valueOf(str.substring(str.indexOf('=') + 1, str.length()));
                        }
                        if (str.startsWith("# StartY")) {
                            sScriptStartY = Integer.valueOf(str.substring(str.indexOf('=') + 1, str.length()));
                            break;
                        }
                        str = reader.readLine();
                    }

                    mPbPlay_1.setProgress((int)(((float)sScriptsNum/MainActivity.mScripts.size())*100));
                    sScriptsNum++;

                    //ScriptIntepreter interpreter = new ScriptIntepreter();
                    mInterpreter = new ScriptIntepreter();
                    mInterpreter.registerInvoker(new AutoMotionScriptInvoker());

                    if (sRepeatOneIndex != -1 && index <= sRepeatOneIndex) {
                        if (index == -1) break;
                        boolean isMatch = false;
                        while (str != null) {
                            if (str.startsWith("# start=")) {
                                if (index == sRepeatOneIndex) {
                                    isMatch = true;
                                    index = -1;
                                    break;
                                } else {
                                    index++;
                                }
                            }
                            str = reader.readLine();
                        }

                        if (isMatch) {
                            export_file = new File(mContext.getApplicationContext().getDir("Data", 0).getAbsolutePath()
                                    + "/exportScript");
                            FileWriter fw = new FileWriter(export_file);

                            BufferedReader reader_temp;
                            FileInputStream fis_temp = new FileInputStream(script);
                            BufferedInputStream in_temp = new BufferedInputStream(fis_temp);
                            reader_temp = new BufferedReader(new InputStreamReader(in_temp, "utf-8"));
                            String str_temp = reader_temp.readLine();

                            while (!str_temp.startsWith("# start=")) {
                                fw.write(str_temp + "\r\n");
                                str_temp = reader_temp.readLine();
                            }

                            reader_temp.close();
                            fis_temp.close();

                            while (!str.startsWith("# end=")) {
                                fw.write(str + "\r\n");
                                str = reader.readLine();
                            }
                            fw.write(str + "\r\n");

                            fw.close();

                            mInterpreter.loadFile(export_file);
                            mInterpreter.setSpeed(1.0f);
                            mInterpreter.run();

                            Message msg = new Message();
                            msg.what = MSG_SHOW_EXPORT;
                            mRunDoneHandler.sendMessage(msg);
                        }
                    } else {
                        mInterpreter.loadFile(script);
                        mInterpreter.setSpeed(1.0f);
                        mInterpreter.run();
                    }
                }

                mPbPlay_1.setProgress(100);
            }
            catch(Exception e) {
                Log.e("Exception when sendPointerSync", e.toString());
            }

            Message msg = new Message();
            msg.what = MSG_SCRIPT_RUN_DONE;
            mRunDoneHandler.sendMessage(msg);
        }
    };

    public static Handler mRunDoneHandler = new Handler() {
        public void handleMessage(Message msg) {
            if (MSG_SCRIPT_RUN_DONE == msg.what) {
                mBtnExpand.setEnabled(true);
                mBtnPlay.setEnabled(true);
                mBtnResize.setEnabled(true);
                mBtnPlay.setImageResource(R.drawable.ic_action_play);
                mCanvasView.sIsRecording = false;
                mRunScriptThread = null;
                mInterpreter = null;
            }
            else if (MSG_UPDATE_WINDOW_TITLE == msg.what) {
                String title = msg.getData().getString("test_case");
                MainActivity.mPreviewWindow.setTitle(title);
                if (GridViewCustomAdapter.mPrefixMap.containsKey(title)) {
                    MainActivity.mEdtInput.setText(GridViewCustomAdapter.mPrefixMap.get(title));
                    MainActivity.mEdtInput.setSelection(MainActivity.mEdtInput.getText().length());
                }
            }
            else if (MSG_SHOW_EXPORT == msg.what) {
                mImgExport.setVisibility(View.VISIBLE);
            }
        }
    };

    private void saveScript() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String file_name = sdf.format(Calendar.getInstance().getTime())
                + "_" + mContext.getString(R.string.default_script_name);

        File saving_file = new File(Environment.getExternalStorageDirectory().getPath()
                + "/" + file_name);

        try {
            FileWriter fw = new FileWriter(saving_file);

            try {
                FileInputStream fis = new FileInputStream(export_file);
                BufferedReader br = new BufferedReader(new InputStreamReader(fis));
                for (String line = br.readLine(); null != line; line = br.readLine()) {
                    fw.write(line);
                    fw.write("\r\n");
                }
                br.close();
                fis.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }

            fw.flush();
            fw.close();

            MediaScannerConnection.scanFile(mContext,
                    new String[] { saving_file.getAbsolutePath() }, null, null);

            Toast.makeText(mContext,
                    mContext.getString(R.string.save_file_to) + " " + saving_file.getAbsolutePath(),
                    Toast.LENGTH_LONG).show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showSavingDialog() {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.saving_script, null);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
        String file_name = sdf.format(Calendar.getInstance().getTime()) + "_" + mContext.getString(R.string.default_script_name);

        mScriptName = (EditText) view.findViewById(R.id.edt_saving_script);
        mScriptName.setText(file_name);

        AlertDialog.Builder alert = new AlertDialog.Builder(mContext, android.R.style.Theme_DeviceDefault_Light_Dialog);
        alert.setTitle(mContext.getResources().getString(R.string.saving_dialog_title));
        alert.setView(view);
        alert.setCancelable(false);
        alert.setPositiveButton(mContext.getResources().getString(R.string.done),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                File saving_file = new File(Environment.getExternalStorageDirectory().getPath()
                    + "/" + mScriptName.getText().toString());

                try {
                    FileWriter fw = new FileWriter(saving_file);

                    try {
                        FileInputStream fis = new FileInputStream(export_file);
                        BufferedReader br = new BufferedReader(new InputStreamReader(fis));
                        for (String line = br.readLine(); null != line; line = br.readLine()) {
                            fw.write(line);
                            fw.write("\r\n");
                        }
                        br.close();
                        fis.close();
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }

                    fw.flush();
                    fw.close();

                    MediaScannerConnection.scanFile(mContext,
                            new String[] { saving_file.getAbsolutePath() }, null, null);

                    Toast.makeText(mContext,
                            mContext.getString(R.string.save_file_to) + " " + saving_file.getAbsolutePath(),
                            Toast.LENGTH_LONG).show();

                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        alert.setNegativeButton(mContext.getResources().getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog savingDialog = alert.create();

        Window window = savingDialog.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.token = getContentView().getWindowToken();
        lp.type = WindowManager.LayoutParams.TYPE_APPLICATION_ATTACHED_DIALOG;
        lp.dimAmount=0.6f;
        window.setAttributes(lp);
        window.addFlags(WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        savingDialog.show();
    }
}

