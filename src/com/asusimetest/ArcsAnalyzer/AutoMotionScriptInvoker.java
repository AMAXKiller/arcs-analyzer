package com.asusimetest.ArcsAnalyzer;

import android.app.Instrumentation;
import android.os.SystemClock;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;

import java.lang.Exception;
import java.lang.RuntimeException;
import java.lang.StringBuffer;

public class AutoMotionScriptInvoker implements ScriptIntepreter.CommandInvoker {

    private static final String TAG = "AutoMotionScriptInvoker";
    private long beginTime = 0;

    @Override
    public boolean canSupport(ScriptIntepreter.ScriptCommand _cmd) {
        String[] cmdParas = _cmd.getCommandParameters();
        if (cmdParas[0].equals("reset_time")) return true;
        if (cmdParas[0].equals("wait_till")) return true;
        if (cmdParas[0].equals("touch_up"))	return true;
        if (cmdParas[0].equals("touch_down")) return true;
        if (cmdParas[0].equals("touch_move")) return true;
        if (cmdParas[0].equals("print")) return true;
        if (cmdParas[0].equals("goto_home")) return true;
        return false;
    }

    @Override
    public void preprocess(ScriptIntepreter.ScriptCommand _cmd) { }

    private void cmdSleep(long _msTime) { try {Thread.sleep(_msTime);} catch (Exception e) {} }

    public void run(ScriptIntepreter.ScriptCommand _cmd) {
        String[] cmdParas = _cmd.getCommandParameters();
        Log.d(TAG, "cmd = " + cmdParas[0] + " x = " + _cmd.getParameterValue(1) + " y = " + _cmd.getParameterValue(2));
        try {
            if (cmdParas[0].equals("print")) {
                StringBuffer sb = new StringBuffer();
                for (int i=1; i < cmdParas.length; i++) {
                    sb.append(_cmd.isParameterWithValue(i)? _cmd.getParameterValue(i) : cmdParas[i]);
                }
                Log.d(TAG, sb.toString());
            }
            else if (cmdParas[0].equals("touch_down")) {
                new Instrumentation().sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(),
                        SystemClock.uptimeMillis(),
                        MotionEvent.ACTION_DOWN,
                        handleX(_cmd.getParameterValue(1)),
                        handleY(_cmd.getParameterValue(2)),
                        0));
            }
            else if (cmdParas[0].equals("touch_up")) {
                new Instrumentation().sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(),
                        SystemClock.uptimeMillis(),
                        MotionEvent.ACTION_UP,
                        handleX(_cmd.getParameterValue(1)),
                        handleY(_cmd.getParameterValue(2)),
                        0));
            }
            else if (cmdParas[0].equals("touch_move")) {
                new Instrumentation().sendPointerSync(MotionEvent.obtain(SystemClock.uptimeMillis(),
                        SystemClock.uptimeMillis(),
                        MotionEvent.ACTION_MOVE,
                        handleX(_cmd.getParameterValue(1)),
                        handleY(_cmd.getParameterValue(2)),
                        0));
            }
            else if (cmdParas[0].equals("wait_till")) {
                cmdSleep(SystemClock.uptimeMillis() - beginTime - (long) _cmd.getParameterValue(1));
            }
            else if (cmdParas[0].equals("reset_time")) {
                beginTime = SystemClock.uptimeMillis();
            }
            else if (cmdParas[0].equals("goto_home")) {
                new Instrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_HOME);
            }
            else {
                throw new RuntimeException(TAG+": Unknow command " + _cmd + " at line "+ _cmd.getLineNumInSource());
            }
        } catch (Exception e) {}
    }

    private int handleX(double original_x) {
        return (int)((double)(original_x
                - PreviewWindow.sScriptStartX)
                * MainActivity.mPreviewWindow.getPreviewViewWidth()
                / PreviewWindow.sScriptPreviewViewWidth
                + MainActivity.mCurrentX);
    }

    private int handleY(double original_y) {
        if (!PreviewWindow.sIsCanvasHide) {
            return (int)(
                    (MainActivity.mPreviewWindow.getPreviewViewHeight()
                    - ((double)(PreviewWindow.sScriptPreviewViewHeight
                    + PreviewWindow.sScriptStartY - original_y)
                    * MainActivity.mPreviewWindow.getPreviewViewWidth()
                    / PreviewWindow.sScriptPreviewViewWidth))
                    + MainActivity.mCurrentY);
        } else {
            return (int)(
                    (MainActivity.mPreviewWindow.getPreviewViewHeight()
                    + MainActivity.mPreviewWindow.getPreviewViewWidth()
                    - ((double)(PreviewWindow.sScriptPreviewViewHeight
                    + PreviewWindow.sScriptStartY - original_y)
                    * MainActivity.mPreviewWindow.getPreviewViewWidth()
                    / PreviewWindow.sScriptPreviewViewWidth))
                    + MainActivity.mCurrentY);
        }
    }
}
